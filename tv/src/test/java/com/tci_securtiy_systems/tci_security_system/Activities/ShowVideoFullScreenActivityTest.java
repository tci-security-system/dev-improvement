package com.tci_securtiy_systems.tci_security_system.Activities;

/**
 * Created by glenjeffreympoyonkali on 2017-10-08.
 */

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class ShowVideoFullScreenActivityTest {

    @Test
    public void ShowVideoUrlIsCorrect() throws Exception {
        assertEquals(ShowVideoFullScreenActivity.VIDEO_URL, "http://www.androidbegin.com/tutorial/AndroidCommercial.3gp");
    }
}

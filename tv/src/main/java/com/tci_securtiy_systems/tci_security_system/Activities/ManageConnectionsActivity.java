package com.tci_securtiy_systems.tci_security_system.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.tci_securtiy_systems.tci_security_system.Entities.ConnectionItem;
import com.tci_securtiy_systems.tci_security_system.R;

import java.util.ArrayList;

// Code for dynamic list functionality obtained from Stack Overflow
// URL: https://stackoverflow.com/questions/22144891/how-to-add-listview-items-on-button-click-using-adapter
public class ManageConnectionsActivity extends Activity {
    EditText name;
    Button enableButton;
    Button addButton;
    int lastSelected;
    Button deleteButton;
    LinearLayout connectLayout;
    ListView connectedDevices;
    ArrayList<ConnectionItem> listItems;
    ArrayList<String> displayedList;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_connections);
        name = (EditText) findViewById(R.id.nameText);
        enableButton = (Button) findViewById(R.id.enableAddButton);
        addButton = (Button) findViewById(R.id.addDeviceButton);
        lastSelected = 10000;
        deleteButton = (Button) findViewById(R.id.removeConnectionButton);
        connectLayout = (LinearLayout) findViewById(R.id.connectLayout);
        connectedDevices = (ListView) findViewById(R.id.deviceList);
        listItems = getListData();
        displayedList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                displayedList);
        connectedDevices.setAdapter(adapter);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addItem();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                removeItem();
            }
        });
        enableButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                enableAdd();
            }
        });
        connectedDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                lastSelected = position;
                String text = listItems.get(lastSelected).getConnectionName();
                Toast.makeText(ManageConnectionsActivity.this, "Connection \"" + text + "\" selected.", Toast.LENGTH_SHORT).show();
            }
        });

        for (int idx=0; idx<connectLayout.getChildCount(); idx++) {
            (connectLayout.getChildAt(idx)).setEnabled(false);
        }
    }

    private void addItem() {
        EditText nameText = (EditText) findViewById(R.id.nameText);
        if (!(nameText.getText().toString()).equals("")) {
            ConnectionItem newConnection = new ConnectionItem();
            newConnection.setConnectionName(((EditText) findViewById(R.id.nameText)).getText().toString());
            newConnection.setIpAddress(((EditText) findViewById(R.id.addressText)).getText().toString());
            newConnection.setPort(((EditText) findViewById(R.id.portText)).getText().toString());
            newConnection.setUsername(((EditText) findViewById(R.id.usernameText)).getText().toString());
            newConnection.setPassword(((EditText) findViewById(R.id.passwordText)).getText().toString());
            newConnection.setLiveView(((EditText) findViewById(R.id.liveviewText)).getText().toString());
            newConnection.setNumCameras(14);
            listItems.add(newConnection);
            displayedList.add(newConnection.getConnectionName() + " (" + newConnection.getNumCameras() + ")");
            adapter.notifyDataSetChanged();
        }
        for (int idx=0; idx<connectLayout.getChildCount(); idx++) {
            View thisChild = connectLayout.getChildAt(idx);
            if (thisChild instanceof EditText) {
                ((EditText)thisChild).setText("");
            }
            connectLayout.getChildAt(idx).setEnabled(false);
        }
    }

    public void removeItem() {
        if (lastSelected < 10000) {
            String text = listItems.get(lastSelected).getConnectionName();
            listItems.remove(lastSelected);
            displayedList.remove(lastSelected);
            adapter.notifyDataSetChanged();
            Toast.makeText(ManageConnectionsActivity.this, "Connection \"" + text + "\" removed.", Toast.LENGTH_SHORT).show();
            lastSelected = 10000;
        }
    }

    public void enableAdd(){
        for (int idx=0; idx<connectLayout.getChildCount(); idx++) {
            (connectLayout.getChildAt(idx)).setEnabled(true);
        }
    }

    private ArrayList<ConnectionItem> getListData() {
        ArrayList<ConnectionItem> results = new ArrayList<ConnectionItem>();
        return results;
    }
}

package com.tci_securtiy_systems.tci_security_system.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.tci_securtiy_systems.tci_security_system.R;

/**
 * Created by glenjeffreympoyonkali on 2017-09-30.
 */

public class ShowVideoFullScreenActivity extends Activity {
    ProgressDialog pd;
    VideoView view;
    public static final String VIDEO_URL = "http://www.androidbegin.com/tutorial/AndroidCommercial.3gp";
    /* (non-Javadoc) * @see android.app.Activity#onCreate(android.os.Bundle)*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_fullscreen);
        view = (VideoView)findViewById(R.id.camerafullscreen_videoView);
        pd = new ProgressDialog(ShowVideoFullScreenActivity.this);
        pd.setTitle("Video Streaming Demo");
        pd.setMessage("Buffering...");
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.show();

        try{

            MediaController controller = new MediaController(ShowVideoFullScreenActivity.this);
            controller.setAnchorView(view);
            Uri vid = Uri.parse(VIDEO_URL);
            view.setMediaController(controller);
            view.setVideoURI(vid);
        }catch(Exception e){
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        view.requestFocus();
        view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                // TODO Auto-generated method stub
                pd.dismiss();
                view.start();
            }
        });
    }
}

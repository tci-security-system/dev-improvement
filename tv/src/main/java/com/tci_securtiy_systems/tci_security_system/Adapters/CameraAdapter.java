package com.tci_securtiy_systems.tci_security_system.Adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.VideoView;

import com.tci_securtiy_systems.tci_security_system.Activities.ShowVideoFullScreenActivity;
import com.tci_securtiy_systems.tci_security_system.R;

/**
 * Created by mohamadou on 2017-09-30.
 */

public class CameraAdapter extends BaseAdapter {

    private Context mContext;
    private final String[] web;
    private final int[] Imageid;

    public CameraAdapter(Context c,String[] web,int[] Imageid ) {
        mContext = c;
        this.Imageid = Imageid;
        this.web = web;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return web.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.single_video_item, null);
            TextView textView = (TextView) grid.findViewById(R.id.cameraid_textview);
            VideoView videoView = (VideoView)grid.findViewById(R.id.singleCamera_videoView);

            textView.setText(web[position]);
            //no need to set a media controller because in the multiple camera view the video
            //should not be controllable
            Uri vid = Uri.parse(ShowVideoFullScreenActivity.VIDEO_URL);
            videoView.setVideoURI(vid);
            //because we start the video without waiting for the return of a processDialog
            //initially some videos will have a black background while there are loading
            videoView.start();
        } else {
            grid = (View) convertView;
        }

        return grid;
    }

    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
            R.drawable.camera,
    };
}
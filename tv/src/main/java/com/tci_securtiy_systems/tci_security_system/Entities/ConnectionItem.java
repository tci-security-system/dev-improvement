package com.tci_securtiy_systems.tci_security_system.Entities;

public class ConnectionItem {
    private String connectionName;
    private String ipAddress;
    private String port;
    private String username;
    private String password;
    private String liveView;
    private int numCameras;

    public void setConnectionName(String newConnection) {
        connectionName = newConnection;
    }

    public void setIpAddress(String newIPAddress) {
        ipAddress = newIPAddress;
    }

    public void setPort(String newPort) {
        port = newPort;
    }

    public void setUsername(String newUsername) {
        username = newUsername;
    }

    public void setPassword(String newPassword) {
        password = newPassword;
    }

    public void setLiveView(String newLiveView) {
        liveView = newLiveView;
    }

    public void setNumCameras(int newCameras) {
        numCameras = newCameras;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public int getNumCameras() {
        return numCameras;
    }
}
